FROM node:20-alpine as base

ENV NODE_ENV=production

WORKDIR /app

# Install deps
COPY ./package*.json ./
RUN npm ci
COPY . .

CMD npm run start
