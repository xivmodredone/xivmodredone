import { appLogger } from '@/lib/logger';
import { initBull, initCrons } from '@/lib/bull';

import { upsertCollection as upsertModsCollection } from '@/models/mods';

const initModels = () => {
  const logger = appLogger.child({ scope: 'init.models' });
  return Promise.all([
    upsertModsCollection().then((collection) => logger.verbose(`Mods collection upserted (${collection?.num_documents} documents)`)),
  ]);
};

const logger = appLogger.child({ scope: 'init' });

try {
  await initModels();
  logger.info('All models initialized');

  await initBull();
  logger.info('Bull initialized');

  await initCrons();
  logger.info('Crons initiliazed');
} catch (error) {
  const err = error as Error;
  logger.error({
    name: err.name,
    message: err.message,
    stack: err.stack,
  });
}
