import type { CollectionCreateSchema } from 'typesense/lib/Typesense/Collections';
import type { SearchParams, SearchOptions } from 'typesense/lib/Typesense/Documents';
import { HTTPError } from 'typesense/lib/Typesense/Errors';

import { typesense, applyMigrations, createCollection } from '@/lib/typesense';

export enum ModSources {
  XMA = 'XMA',
}

export type ModData = {
  id: string,
  from: ModSources,
  'from.id': string,
  title: string,
  description: string,
  contributorInfo: string,
  author: string,
  version: string,
  category: string,
  isNSFW: boolean,
  createdAt: number,
  updatedAt: number,
  urls: {
    original: string,
    downloads: string[],
    previews: string[],
  },
  meta: {
    races: string[],
    genders: string[],
    tags: string[],
    affects: string,
  },
};

const schema: CollectionCreateSchema = {
  name: 'mods',
  fields: [
    {
      name: 'id',
      type: 'string',
    },
    {
      name: 'from',
      type: 'string',
    },
    {
      name: 'from.id',
      type: 'string',
      sort: true,
    },
    {
      name: 'title',
      type: 'string',
    },
    {
      name: 'description',
      type: 'string',
      facet: true,
    },
    {
      name: 'contributorInfo',
      type: 'string',
      index: false,
      optional: true,
    },
    {
      name: 'author',
      type: 'string',
      facet: true,
    },
    {
      name: 'version',
      type: 'string',
      index: false,
      optional: true,
    },
    {
      name: 'isNSFW',
      type: 'bool',
    },
    {
      name: 'category',
      type: 'string',
    },
    {
      name: 'createdAt',
      type: 'int64',
    },
    {
      name: 'updatedAt',
      type: 'int64',
      optional: true,
    },
    {
      name: 'urls.original',
      type: 'string',
      index: false,
      optional: true,
    },
    {
      name: 'urls.downloads',
      type: 'string[]',
      index: false,
      optional: true,
    },
    {
      name: 'urls.previews',
      type: 'string[]',
      index: false,
      optional: true,
    },
    {
      name: 'meta.races',
      type: 'string[]',
      optional: true,
    },
    {
      name: 'meta.genders',
      type: 'string[]',
      optional: true,
    },
    {
      name: 'meta.tags',
      type: 'string[]',
      optional: true,
    },
    {
      name: 'meta.affects',
      type: 'string',
      optional: true,
    },
  ],
  default_sorting_field: 'createdAt',
};

type Fields = 'id'
| 'from'
| 'from.id'
| 'title'
| 'description'
| 'contributorInfo'
| 'author'
| 'version'
| 'isNSFW'
| 'category'
| 'createdAt'
| 'updatedAt'
| 'urls.original'
| 'urls.downloads'
| 'urls.previews'
| 'meta.races'
| 'meta.genders'
| 'meta.tags'
| 'meta.affects';

type SortableFields = 'from.id' | 'isNSFW' | 'createdAt' | 'updatedAt';
type IndexedFields = Exclude<Fields, 'contributorInfo' | 'version' | 'urls.original' | 'urls.downloads' | 'urls.previews'>;

const collection = typesense.collections<ModData>(schema.name);

type CustomSortParameter = SortableFields | `${SortableFields}:${'asc' | 'desc'}`;
type CustomSearchParameters = SearchParams & {
  query_by?: Exclude<IndexedFields, 'id'>,
  filter_by?: `${IndexedFields}:${string}`,
  sort_by?: CustomSortParameter | `${CustomSortParameter},${CustomSortParameter}` | `${CustomSortParameter},${CustomSortParameter},${CustomSortParameter}`,
};
export const search = async (
  searchParameters: CustomSearchParameters,
  opts?: SearchOptions,
) => collection.documents().search(searchParameters, opts);

export async function* searchLatests(count: number, source: ModSources | '*' = '*') {
  let page = 1;
  let dataLength = 0;
  while (dataLength < count) {
    // eslint-disable-next-line no-await-in-loop
    const { hits = [] } = await search({
      sort_by: 'updatedAt:desc,createdAt:desc',
      q: source,
      query_by: 'from',
      per_page: 100,
      page,
    });

    dataLength += hits.length;
    page += 1;

    yield hits.slice(0, count - dataLength);
  }
}

export const upsertCollection = async () => {
  const isCollectionExists = await collection.exists();
  if (isCollectionExists) {
    return applyMigrations(schema);
  }
  return createCollection(schema);
};

export const getCount = async () => (await collection.retrieve()).num_documents;

export const insert = async (data: ModData) => collection.documents().upsert(data, { action: 'upsert' });

export const drop = async (id: ModData['id']) => {
  try {
    return await collection.documents(id).delete();
  } catch (error) {
    if ((error as HTTPError).httpStatus === 404) {
      return null;
    }
    throw error;
  }
};

export const dropMany = async (ids: ModData['id'][]) => collection.documents().delete({
  filter_by: `id: [${ids.join(',')}]`,
});
