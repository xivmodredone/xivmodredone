import db from '@/lib/lowdb';

import { ModSources } from '@/models/mods';

type BannedId = {
  id: string,
  from: ModSources,
};

export const insert = ({ id, from }: BannedId) => {
  db.data.bannedIds[from][id] = true;
  return db.write();
};

export const getSet = async (from: ModSources) => new Set<string>(
  Object.keys(db.data.bannedIds[from]),
);
