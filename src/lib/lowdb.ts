import { JSONFilePreset } from 'lowdb/node';

const defaultData = {
  bannedIds: {
    XMA: {} as Record<string, true>,
  },
};
type Data = typeof defaultData;

const db = await JSONFilePreset<Data>('./db/db.json', defaultData);

export default db;
