import winston from 'winston';
import 'winston-daily-rotate-file';
import type DailyRotateFile from 'winston-daily-rotate-file';

import config from '@/lib/config';

const formatter = (info: winston.Logform.TransformableInfo) => {
  let str = `${info.timestamp} [${info.label}] ${info.level}:`;

  if (info.scope) {
    str += ` [${info.scope}]`;
  }

  str += ` ${info.message}`;

  if (info.stack) {
    str += `\n\n${info.stack}`;
  }

  return str;
};

const defaultDailyRotateOpts = {
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json({}),
  ),
  dirname: 'logs',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '14d',
} satisfies DailyRotateFile.DailyRotateFileTransportOptions;

export const appLogger = winston.createLogger({
  level: config.logLevel,
  transports: [
    new winston.transports.DailyRotateFile({
      ...defaultDailyRotateOpts,
      filename: 'app-%DATE%.jsonl',
    }),
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.label({ label: 'app' }),
        winston.format.timestamp(),
        winston.format.colorize(),
        winston.format.printf(formatter),
      ),
    }),
  ],
});

export const accessLogger = winston.createLogger({
  level: config.logLevel,
  transports: [
    new winston.transports.DailyRotateFile({
      ...defaultDailyRotateOpts,
      filename: 'access-%DATE%.jsonl',
    }),
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.label({ label: 'access' }),
        winston.format.timestamp(),
        winston.format.colorize(),
        winston.format.printf(formatter),
      ),
    }),
  ],
});
