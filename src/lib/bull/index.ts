import { appLogger } from '@/lib/logger';

import { QueueNames, CronMap } from './config';

import HarvestXMAWorker from './harvest.xma/worker';
import CronFullXMAWorker from './cron.full.xma/worker';
import CronNewXMAWorker from './cron.new.xma/worker';

import HarvestXMAQueue from './harvest.xma/queue';
import CronFullXMAQueue from './cron.full.xma/queue';
import CronNewXMAQueue from './cron.new.xma/queue';

const WorkerMap = new Map([
  [QueueNames.HARVEST_XMA, HarvestXMAWorker],

  [QueueNames.CRON_FULL_XMA, CronFullXMAWorker],
  [QueueNames.CRON_NEW_XMA, CronNewXMAWorker],
]);

export const QueueMap = new Map([
  [QueueNames.HARVEST_XMA, HarvestXMAQueue],

  [QueueNames.CRON_FULL_XMA, CronFullXMAQueue],
  [QueueNames.CRON_NEW_XMA, CronNewXMAQueue],
]);

export const initBull = async () => {
  const logger = appLogger.child({ scope: 'bull' });

  for (const [, worker] of WorkerMap) {
    worker.on('error', (err) => logger.error({
      name: err.name,
      message: err.message,
      stack: err.stack,
    }));
  }
};

export const initCrons = async () => {
  const logger = appLogger.child({ scope: 'crons' });

  try {
    await Promise.all(
      [...CronMap].map(async ([queueName, cron]) => {
        const queue = QueueMap.get(queueName);
        if (!queue) {
          throw new Error(`Queue [${queueName}] not found`);
        }

        const jobs = await queue.getRepeatableJobs();

        await Promise.all(
          jobs.map(async (job) => {
            const isRemoved = await queue.removeRepeatable(
              job.name,
              { pattern: job.pattern },
            );

            if (isRemoved) {
              logger.verbose(`Removed existing job [${job.name}] with [${job.pattern}]`);
            }
            return isRemoved;
          }),
        );

        const job = await queue.add(
          queueName,
          undefined,
          { repeat: { pattern: cron } },
        );
        logger.verbose(`Added job [${job.name}] with [${cron}]`);
      }),
    );
  } catch (error) {
    const err = error as Error;
    logger.error({
      name: err.name,
      message: err.message,
      stack: err.stack,
    });
  }
};

export { QueueNames };
