/* eslint-disable import/prefer-default-export */
export const isRateLimitError = (error: Error) => error.message === 'bullmq:rateLimitExceeded';
