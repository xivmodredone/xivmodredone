import { Queue } from 'bullmq';

import { QueueNames, getRedisConnection, queueOptions } from '../config';

const HarvestXMAQueue = new Queue(
  QueueNames.HARVEST_XMA,
  {
    ...queueOptions,
    connection: getRedisConnection(QueueNames.HARVEST_XMA),
    defaultJobOptions: {
      attempts: 3,
      removeOnComplete: true,
      removeOnFail: 2000,
    },
  },
);

export default HarvestXMAQueue;
