import type { SandboxedJob } from 'bullmq';

import HarvestXMAProcessor from './processor';

export default (job: SandboxedJob) => HarvestXMAProcessor.process(job);
