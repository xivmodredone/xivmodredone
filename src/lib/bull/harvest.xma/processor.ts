import { Worker, type SandboxedJob } from 'bullmq';
import type { Logger } from 'winston';

import { appLogger } from '@/lib/logger';
import { getModByIdFromXMA } from '@/lib/xma';

import { insert as insertBannedId } from '@/models/bannedIds';
import {
  type ModData,
  ModSources,
  insert as insertMod,
  drop as dropMod,
} from '@/models/mods';

import Processor from '../processor.base';
import { QueueNames, workerOptions } from '../config';
import { isRateLimitError } from '../utils';

export default class HarvestXMAProcessor extends Processor<ModData | null> {
  private logger: Logger;

  private worker: Worker;

  constructor(job: SandboxedJob) {
    super(job);
    this.logger = appLogger.child({ scope: `harvest.xma:${job.data.id}`, job: job.id });
    this.worker = new Worker(QueueNames.HARVEST_XMA, undefined, workerOptions);
  }

  public async run() {
    const { data } = this.job;
    this.logger.verbose(`Starting harvesting XMA for [${data.id}]`);

    const { data: rawMod, rateLimited, found } = await getModByIdFromXMA(data.id);

    if (rateLimited) {
      await this.worker.rateLimit(rateLimited);
      throw Worker.RateLimitError();
    }

    if (!rawMod) {
      let unavailableType = 'Unknown';
      switch (found) {
        case true:
          unavailableType = 'PrivateOrDraft';
          break;

        case false:
          await insertBannedId({ id: `${data.id}`, from: ModSources.XMA });
          this.logger.info(`Inserted banned id: [${data.id}] from XMA`);
          unavailableType = 'NotFound';
          break;

        default:
          throw new Error(`Unexpected result from XMA: ${unavailableType}`);
      }

      this.logger.verbose(`Mod [${data.id}] is unavailable from XMA now : ${unavailableType}`);
      const mod = await dropMod(`xma:${data.id}`);
      if (mod) {
        this.logger.info({
          message: `Deleted mod: [${mod.id}] "${mod.title}"`,
          mod,
        });
      }
      return mod;
    }

    this.logger.verbose(`Got mod: [${rawMod.id}] "${rawMod.title}"`);
    const mod: ModData = {
      ...rawMod,
      id: `xma:${rawMod.id}`,
      from: ModSources.XMA,
      'from.id': rawMod.id,
    };

    await insertMod(mod);
    this.logger.info({
      message: `Inserted mod: [${mod.id}] "${mod.title}"`,
      mod,
    });

    return mod;
  }

  public async failed(err: Error) {
    if (isRateLimitError(err)) {
      return;
    }
    this.logger.error({
      name: err.name,
      message: err.message,
      stack: err.stack,
    });
  }

  public async completed() {
    await this.worker.disconnect();
  }
}
