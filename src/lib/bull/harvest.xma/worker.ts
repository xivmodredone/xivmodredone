import { Worker } from 'bullmq';

import { resolve } from 'node:path';
import { pathToFileURL } from 'node:url';

import config from '@/lib/config';

import { QueueNames, getRedisConnection, workerOptions } from '../config';

const HarvestXMAWorker = new Worker(
  QueueNames.HARVEST_XMA,
  pathToFileURL(resolve(import.meta.dirname, 'sandbox.ts')),
  {
    ...workerOptions,
    connection: getRedisConnection(QueueNames.HARVEST_XMA),
    limiter: {
      max: 1,
      duration: config.xma.requestDelay,
    },
  },
);

export default HarvestXMAWorker;
