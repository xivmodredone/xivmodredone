import { Queue } from 'bullmq';

import { QueueNames, getRedisConnection, queueOptions } from '../config';

const CronNewXMAQueue = new Queue(
  QueueNames.CRON_NEW_XMA,
  {
    ...queueOptions,
    connection: getRedisConnection(QueueNames.CRON_NEW_XMA),
  },
);

export default CronNewXMAQueue;
