import { Worker, type Job } from 'bullmq';

import { QueueNames, getRedisConnection, workerOptions } from '../config';
import CronNewXMAProcessor from './processor';

const CronNewXMAWorker = new Worker(
  QueueNames.CRON_NEW_XMA,
  (job: Job) => CronNewXMAProcessor.process(job),
  {
    ...workerOptions,
    connection: getRedisConnection(QueueNames.CRON_NEW_XMA),
  },
);

export default CronNewXMAWorker;
