import type { Queue, Job } from 'bullmq';
import type { Logger } from 'winston';

import { appLogger } from '@/lib/logger';
import { getLatestsIdsFromXMA } from '@/lib/xma';

import { getSet as getBannedIdsSet } from '@/models/bannedIds';
import { ModSources, searchLatests as searchLatestsMods } from '@/models/mods';

import Processor from '../processor.base';
import HarvestXMAQueue from '../harvest.xma/queue';

type JobBulk = Parameters<Queue['addBulk']>[0];

const MOD_COUNT = 5 * 60;

export default class CronNewXMAProcessor extends Processor<Job[]> {
  private logger: Logger;

  constructor(job: Job) {
    super(job);
    this.logger = appLogger.child({ scope: 'cron.new.xma', job: job.id });
  }

  public async run() {
    this.logger.verbose('Starting checking updates from XMA');

    const idsToHarvest: string[] = [];
    const getUpdatesFromXMA = async () => {
      for await (const { data } of getLatestsIdsFromXMA(MOD_COUNT)) {
        if (!data) {
          throw new Error('No updates from XMA found');
        }
        this.logger.verbose(`Got ${data.length} ids from XMA`, { ids: data });

        idsToHarvest.push(
          ...data.map((id) => `${id}`),
        );
      }
    };

    const getUpdatesFromMods = async () => {
      for await (const data of searchLatestsMods(MOD_COUNT, ModSources.XMA)) {
        this.logger.verbose(`Got ${data.length} mods from XMA`, { ids: data });

        idsToHarvest.push(
          ...data.map((mod) => mod.document['from.id']),
        );
      }
    };

    await Promise.all([getUpdatesFromXMA(), getUpdatesFromMods()]);

    const bannedIds = await getBannedIdsSet(ModSources.XMA);
    this.logger.verbose(`Got ${bannedIds.size} banned ids from XMA`);

    const idsToHarvestSet = new Set(idsToHarvest.filter((id) => !bannedIds.has(id)));
    this.logger.verbose(`Found ${idsToHarvestSet.size} ids to re-harvest from XMA`, { ids: idsToHarvestSet });

    const jobs: JobBulk = [];
    for (const id of idsToHarvestSet) {
      jobs.push({
        name: `xmamod-${id}`,
        data: { id },
        opts: { lifo: true },
      });
      this.logger.silly(`Added mod ${id} to queue`);
    }

    return HarvestXMAQueue.addBulk(jobs);
  }

  public async failed(err: Error) {
    this.logger.error({
      name: err.name,
      message: err.message,
      stack: err.stack,
    });
  }
}
