/* eslint-disable import/export */
import type { Job, SandboxedJob } from 'bullmq';

interface ProcessorConstructor<U, J extends Job | SandboxedJob> {
  new (job: J): Processor<U>;
}

export default interface Processor {
  completed?(): Promise<void>;
  failed?(error: Error): Promise<void>;
}

export default abstract class Processor<T = void> {
  static async process<
    U,
    J extends Job | SandboxedJob,
    T extends ProcessorConstructor<U, J>,
  >(this: T, job: J) {
    const processor = new this(job);
    try {
      return await processor.run();
    } catch (error) {
      const err = error as Error;
      await processor.failed?.(err);
      throw error;
    } finally {
      await processor.completed?.();
    }
  }

  constructor(protected readonly job: Job | SandboxedJob) {}

  abstract run(): Promise<T>;
}
