import type { Queue, Job } from 'bullmq';
import type { Logger } from 'winston';

import { appLogger } from '@/lib/logger';
import { getGreatestIdFromXMA } from '@/lib/xma';

import { ModSources } from '@/models/mods';
import { getSet as getBannedIdsSet } from '@/models/bannedIds';

import Processor from '../processor.base';
import HarvestXMAQueue from '../harvest.xma/queue';

type JobBulk = Parameters<Queue['addBulk']>[0];

export default class CronFullXMAProcessor extends Processor<Job[]> {
  private logger: Logger;

  constructor(job: Job) {
    super(job);
    this.logger = appLogger.child({ scope: 'cron.full.xma', job: job.id });
  }

  public async run() {
    this.logger.verbose('Starting checking all mods from XMA');

    const { data: maxId } = await getGreatestIdFromXMA();
    if (!maxId) {
      throw new Error('No max id from XMA found');
    }
    this.logger.verbose(`Greatest id of XMA found: ${maxId}`);

    const bannedIds = await getBannedIdsSet(ModSources.XMA);
    this.logger.verbose(`Got ${bannedIds.size} banned ids from XMA`);

    const jobs: JobBulk = [];
    for (let id = maxId; id > 0; id -= 1) {
      if (bannedIds.has(`${id}`)) {
        this.logger.verbose(`Skipping banned mod ${id} from XMA`);
        // eslint-disable-next-line no-continue
        continue;
      }

      jobs.push({
        name: `xmamod-${id}`,
        data: { id },
      });
      this.logger.silly(`Added mod ${id} to queue`);
    }
    return HarvestXMAQueue.addBulk(jobs);
  }

  public async failed(err: Error) {
    this.logger.error({
      name: err.name,
      message: err.message,
      stack: err.stack,
    });
  }
}
