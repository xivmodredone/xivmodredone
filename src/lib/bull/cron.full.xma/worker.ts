import { Worker, type Job } from 'bullmq';

import { QueueNames, getRedisConnection, workerOptions } from '../config';
import CronFullXMAProcessor from './processor';

const CronFullXMAWorker = new Worker(
  QueueNames.CRON_FULL_XMA,
  (job: Job) => CronFullXMAProcessor.process(job),
  {
    ...workerOptions,
    connection: getRedisConnection(QueueNames.CRON_FULL_XMA),
  },
);

export default CronFullXMAWorker;
