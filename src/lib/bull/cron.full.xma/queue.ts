import { Queue } from 'bullmq';

import { QueueNames, getRedisConnection, queueOptions } from '../config';

const CronFullXMAQueue = new Queue(
  QueueNames.CRON_FULL_XMA,
  {
    ...queueOptions,
    connection: getRedisConnection(QueueNames.CRON_FULL_XMA),
  },
);

export default CronFullXMAQueue;
