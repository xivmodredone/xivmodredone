import IORedis from 'ioredis';
import type {
  ConnectionOptions,
  QueueOptions,
  WorkerOptions,
} from 'bullmq';

import config from '@/lib/config';

const connection: ConnectionOptions = {
  ...config.redis,
};

export const queueOptions: QueueOptions = {
  connection,
  defaultJobOptions: {
    removeOnComplete: true,
    removeOnFail: 30,
  },
};

export const workerOptions: WorkerOptions = {
  connection,
  concurrency: config.workers.concurrency,
};

export enum QueueNames {
  HARVEST_XMA = 'harvest.xma',

  CRON_FULL_XMA = 'cron.full.xma',
  CRON_NEW_XMA = 'cron.new.xma',
}

export const CronMap = new Map([
  [QueueNames.CRON_FULL_XMA, config.crons.harvestAllXMA],
  [QueueNames.CRON_NEW_XMA, config.crons.harvestNewXMA],
]);

const RedisConnectionMap = new Map<string, IORedis>();

export const getRedisConnection = (queueName: QueueNames) => {
  let redisConnection = RedisConnectionMap.get(queueName);
  if (!redisConnection) {
    redisConnection = new IORedis({
      ...connection,
      maxRetriesPerRequest: null,
    });
    RedisConnectionMap.set(queueName, redisConnection);
  }
  return redisConnection;
};
