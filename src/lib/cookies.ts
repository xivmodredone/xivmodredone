export type Cookie = {
  name: string,
  value: string,
  domain?: string,
  path?: string,
  expires?: Date,
  httponly?: boolean,
};

/**
 * Parse a raw cookie into a usable object
 *
 * @param rawCookie The cookie extracted from the response
 *
 * @returns The parsed cookie
 */
export const parseCookie = (rawCookie: string): Cookie => {
  const attrs = rawCookie.split(';');
  const [name, ...values] = attrs.shift()?.split('=') ?? [];
  return attrs.reduce(
    (prev, attr) => {
      const [rawKey, ...vs] = attr.split('=');
      let v: string | Date | boolean = vs.join('=').trim();
      const key = rawKey.trim().toLowerCase();

      if (key === 'expires') {
        v = new Date(v);
      }

      if (v === '') {
        v = true;
      }

      return { ...prev, [key]: v };
    },
    { name, value: values.join('=').trim() },
  );
};
