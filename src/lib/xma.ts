import axios, { type AxiosRequestConfig, type AxiosResponse } from 'axios';
import { JSDOM } from 'jsdom';
import sanitize from 'sanitize-html';
import { getUnixTime } from 'date-fns';

import { setTimeout } from 'node:timers/promises';

import pckg from '../../package.json';
import config from './config';
import type { ModData } from '../models/mods';
import { parseCookie, type Cookie } from './cookies';
import { appLogger } from './logger';
import { HTTPError } from './errors';

const MODLINK_ID_REGEX = /\/modid\/(\d+)$/i;
const USERLINK_ID_REGEX = /\/user\/(\d+)$/i;
const VERSION_REGEX = /^Version: ([\d.]+)$/i;
const CATEGORY_REGEX = /A (?<nsfw>NSFW )?(?<categ>.*?) (?:Mod|by)/i;
const FILELINK_REGEX = /^(?<label>.+) : .*href="(?<url>.+)"/i;
const HEADER_REGEX = /^(.+):/i;

type XMAResponse = {
  dom: Document,
  rateLimited?: number,
  response: AxiosResponse
};

type XMAResult<T> = Promise<{
  data?: T,
  rateLimited?: XMAResponse['rateLimited'],
  found?: boolean,
}>;

type ModFile = {
  label: string,
  url: string,
};

/**
 * Cookie used to auth into XMA
 */
let authCookie: Cookie = {
  name: 'connect.sid',
  value: '',
};

const logger = appLogger.child({ scope: 'xma' });

/**
 * Axios instance linked to XMA
 */
const xma = axios.create({
  baseURL: config.xma.baseURL,
  withCredentials: true,
  responseType: 'text',
  validateStatus: (s) => s >= 200 || s < 300 || s === 403 || s === 404,
  headers: {
    'User-Agent': `XIVModRedone-${pckg.version}`,
  },
  transformRequest: [
    // Add auth cookies
    (data, headers) => {
      const cookies = Array.isArray(headers.Cookie) ? headers.Cookie : [headers.Cookie];
      // eslint-disable-next-line no-param-reassign
      headers.Cookie = [
        ...cookies,
        `${authCookie.name}=${authCookie.value}`,
      ];

      return data;
    },
  ],
});

const fetchXMA = async (
  url: string,
  opts?: { axios?: AxiosRequestConfig, throwOnNotFound?: boolean },
): Promise<XMAResponse> => {
  const response = await xma(url, opts?.axios);
  const { data, status, headers } = response;

  const okStatuses = new Set([429]);
  if (opts?.throwOnNotFound === false) {
    okStatuses.add(403);
    okStatuses.add(404);
  }

  let rateLimited: number | undefined;
  const dom = new JSDOM(data).window.document;
  if (status >= 300 && !okStatuses.has(status)) {
    let message = dom.querySelector('.display-5.text-center')?.innerHTML || `Request failed with status ${status}`;

    if (status === 404) {
      message += `. ${dom.querySelector('.lead.text-center')?.innerHTML || ''}`;
    }

    throw new HTTPError(message, status);
  }

  if (status === 429) {
    rateLimited = (headers['retry-after'] ?? 3600) * 1000;
    appLogger.warn(`[xma] Rate limited for ${rateLimited} ms`);
  }

  return {
    rateLimited,
    dom,
    response,
  };
};

/**
 * Checks if cookie used to be authed on XMA is set
 *
 * @returns Is cookie set
 */
const isLoggedToXMA = (): boolean => {
  if (!authCookie.expires) {
    return false;
  }

  return authCookie.expires > new Date();
};

/**
 * Login to XMA in order to see all mods (even NSFW ones)
 */
const loginToXMA = async () => {
  // We're already logged in
  if (isLoggedToXMA()) {
    return { rateLimited: undefined };
  }

  // Get base cookie
  const { response, rateLimited } = await fetchXMA('/');
  if (rateLimited) {
    return { rateLimited };
  }

  const cookies = response.headers['set-cookie'];
  if (!cookies || cookies.length <= 0) {
    throw new Error("Can't get anon cookie");
  }

  // Parse cookies
  const parsedCookies = cookies.map((c) => parseCookie(c));

  // Extract auth cookie
  const authCookieIndex = parsedCookies.findIndex(({ name }) => name === authCookie.name);
  if (authCookieIndex < 0) {
    throw new Error('Cannot get anon token');
  }
  const [extracted] = parsedCookies.splice(authCookieIndex, 1);
  authCookie = extracted;

  // Upgrade cookie as anon
  const { rateLimited: newRateLimited } = await fetchXMA('/anon_login');
  return { rateLimited: newRateLimited };
};

export async function* getLatestsIdsFromXMA(
  count: number,
  opts?: { includeUpdates?: boolean },
): AsyncGenerator<Awaited<XMAResult<number[]>>, void> {
  const loggedData = await loginToXMA();
  if (loggedData.rateLimited) {
    yield { rateLimited: loggedData.rateLimited };
    return;
  }

  const sortBy = opts?.includeUpdates ? 'time_edited' : 'time_posted';
  let page = 1;

  let dataLength = 0;
  while (dataLength < count) {
    // eslint-disable-next-line no-await-in-loop
    const { dom, rateLimited } = await fetchXMA(`/search?sortby=${sortBy}&page=${page}`);
    if (rateLimited) {
      yield { rateLimited };
      return;
    }

    const modsLinks = [...dom.querySelectorAll('.mod-card a').values()];
    const ids = modsLinks
      // Extract id from URL
      .map((e) => {
        const href = e.getAttribute('href');
        const id = MODLINK_ID_REGEX.exec(href || '')?.[1];
        return Number.parseInt(id || '', 10);
      })
      // Remove errors
      .filter((id) => !Number.isNaN(id));

    dataLength += ids.length;
    page += 1;

    yield { data: ids.slice(0, count - dataLength), found: true };

    // eslint-disable-next-line no-await-in-loop
    await setTimeout(config.xma.requestDelay);
  }
}

/**
 * Try to get greatest id of XMA
 *
 * @returns The greatest id
 */
export const getGreatestIdFromXMA = async (): XMAResult<number> => {
  const loggedData = await loginToXMA();
  if (loggedData.rateLimited) {
    return { rateLimited: loggedData.rateLimited };
  }

  const { dom, rateLimited } = await fetchXMA('/search?sortby=time_posted&page=1');
  if (rateLimited) {
    return { rateLimited };
  }

  const modsLinks = [...dom.querySelectorAll('.mod-card a').values()];
  const ids = modsLinks
    // Extract id from URL
    .map((e) => {
      const href = e.getAttribute('href');
      const id = MODLINK_ID_REGEX.exec(href || '')?.[1];
      return Number.parseInt(id || '', 10);
    })
    // Remove errors
    .filter((id) => !Number.isNaN(id));

  return { data: Math.max(...ids) };
};

/**
 * Get ModData from XMA
 *
 * @param id The id of the mod
 * @returns The parsed ModData
 */
export const getModByIdFromXMA = async (id: string | number): XMAResult<Omit<ModData, 'from' | 'from.id'>> => {
  const loggedData = await loginToXMA();
  if (loggedData.rateLimited) {
    return { rateLimited: loggedData.rateLimited };
  }

  const { dom, rateLimited, response } = await fetchXMA(`/modid/${id}`, { throwOnNotFound: false });
  if (rateLimited) {
    return { rateLimited };
  }

  if (response.status === 403) {
    return { found: true };
  }

  if (response.status === 404) {
    return { found: false };
  }

  // Extract title
  const title = (dom.querySelector('div.row.no-gutter div.col-9 h1'))?.innerHTML?.trim();
  if (!title) {
    throw new Error(`No title found for mod [${id}]`);
  }

  // Extract infos
  const infos = [
    ...dom.querySelectorAll('.tab-content .tab-pane#info p.lead + div'),
  ] // Description is often there, so we put it in first
    .reverse()
    // We sanitize the HTML, just to be safe
    .map(
      (el) => sanitize(
        el.innerHTML?.trim() || '',
        {},
      ) || '',
    );
  // Extract files
  const files = [
    dom.querySelector('.tab-content .tab-pane#files primary-download-listing'),
    ...dom.querySelectorAll('.tab-content .tab-pane#files li'),
  ].map(
    (el) => {
      if (!el) { return undefined; }
      const matches = FILELINK_REGEX.exec(el.innerHTML?.trim());
      if (!matches?.groups) {
        return undefined;
      }

      let { url } = matches.groups;
      if (url[0] === '/') {
        url = `${config.xma.baseURL}${url}`;
      }
      return {
        label: matches.groups.label,
        url,
      };
    },
  ).filter((v) => !!v) as ModFile[];

  // Extract author
  const authorLink = dom.querySelector('a.user-card-link')?.getAttribute('href')?.trim();
  const author = USERLINK_ID_REGEX.exec(authorLink ?? '')?.[1];
  if (!author) {
    throw new Error(`No author found for mod [${id}]`);
  }

  // Extract version
  const versionText = dom.querySelector('div.row.no-gutter .col-3.text-right code.text-light')?.innerHTML?.trim();
  let version = VERSION_REGEX.exec(versionText ?? '')?.[1];
  if (!version) {
    version = 'Unknown';
    logger.warn(`Cannot get version of mod [${id}]`);
  }

  // Extract NSFW and category
  let isNSFW = true;
  let category = 'Unknown';
  const categoryText = (dom.querySelector('div.row.no-gutter .col-8 p.lead'))?.innerHTML?.trim();
  const catRegRes = CATEGORY_REGEX.exec(categoryText ?? '');
  if (catRegRes?.groups) {
    isNSFW = !!catRegRes.groups.nsfw;
    category = catRegRes.groups.categ;
  }

  // Extract images
  const previews = [...dom.querySelectorAll('.carousel-item .mod-carousel-image')]
    // Only src is relevant
    .map((e) => e.getAttribute('data-src') || e.getAttribute('src'))
    // Remove errors
    .filter((v) => !!v) as string[];

  // Extract meta
  // let contributors: string[] = [];
  let createdAt = new Date(NaN);
  let updatedAt = new Date(NaN);
  let races: string[] = [];
  let genders: string[] = [];
  let tags: string[] = [];
  let affects: string = 'Unknown';
  dom.querySelectorAll('.mod-meta-block').forEach(
    (el) => {
      const key = HEADER_REGEX.exec(el.innerHTML)?.[1]?.trim();
      const value = el.querySelector('code');
      if (!key || !value?.innerHTML) {
        return;
      }

      // if (key === 'Contributors') {
      //   contributors = [
      //     ...value.querySelectorAll('a'),
      //   ].map((e) => {
      //     const url = e.getAttribute('href');
      //     return USERLINK_ID_REGEX.exec(url ?? '')?.[1];
      //   }).filter((i) => !!i) as string[];
      // }

      switch (key) {
        case 'Races': {
          races = [
            ...value.querySelectorAll('a'),
          ].map((e) => e.innerHTML?.trim())
            .filter((v) => !!v);
          break;
        }
        case 'Genders': {
          genders = [
            ...value.querySelectorAll('a'),
          ].map((e) => e.innerHTML?.trim())
            .filter((v) => !!v);
          break;
        }
        case 'Tags': {
          tags = [
            ...value.querySelectorAll('a'),
          ].map((e) => e.innerHTML?.trim())
            .filter((v) => !!v);
          break;
        }

        case 'Original Release Date': {
          createdAt = new Date(value.innerHTML.trim());
          break;
        }
        case 'Last Version Update': {
          updatedAt = new Date(value.innerHTML.trim());
          break;
        }

        case 'Affects': {
          affects = value.innerHTML.trim() || affects;
          break;
        }

        default:
          break;
      }
    },
  );

  return {
    data: {
      id: id.toString(),
      title,
      description: infos[0] ?? '',
      contributorInfo: infos[1],
      author,
      version,
      category,
      isNSFW,
      createdAt: getUnixTime(createdAt),
      updatedAt: getUnixTime(updatedAt),
      urls: {
        original: `${xma.defaults.baseURL}/modid/${id}`,
        downloads: files.map((v) => JSON.stringify(v)),
        previews,
      },
      meta: {
        races,
        genders,
        tags,
        affects,
      },
    },
  };
};
