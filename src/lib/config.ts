import config from 'config';

import type defaultConfig from '../../config/default.json';

export default config as unknown as typeof defaultConfig;
