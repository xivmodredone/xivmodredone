import hash from 'object-hash';
import { Client } from 'typesense';
import type { CollectionCreateSchema } from 'typesense/lib/Typesense/Collections';

import config from '@/lib/config';
import { appLogger } from '@/lib/logger';

const {
  apiKey,
  protocol,
  host,
  port,
} = config.typesense;

const logger = appLogger.child({ scope: 'typesense' });

export const typesense = new Client({
  apiKey,
  nodes: [
    {
      protocol,
      host,
      port,
    },
  ],
});

try {
  const health = await typesense.health.retrieve();
  logger.verbose({
    message: 'Connected',
    health,
    connection: {
      protocol,
      host,
      port,
    },
  });
} catch (error) {
  const err = error as Error;
  logger.error({
    name: err.name,
    message: err.message,
    stack: err.stack,
  });
}

export const createCollection = async (
  schema: CollectionCreateSchema,
  from?: CollectionCreateSchema,
) => {
  const now = new Date();
  const date = now.toISOString().split('T')[0];
  const name = `${schema.name}_${date}`;
  const collection = await typesense.collections().create({ ...schema, name }, {});

  if (from) {
    const data = await typesense.collections(from.name).documents().export();
    await typesense.collections(name).documents().import(data);
    logger.info(`Migration for ${schema.name} done`);
  }

  await typesense.aliases().upsert(schema.name, { collection_name: name });

  return collection;
};

const hashCreateSchemaFields = new Set([
  'name',
  'default_sorting_field',
  'symbols_to_index',
  'token_separators',
  'enable_nested_fields',
]);
const sortableTypes = new Set(['bool', 'int64', 'int32', 'float']);

const hashCreateSchema = ({ fields, ...schema }: CollectionCreateSchema) => {
  const schemaHash = hash(
    {
      enable_nested_fields: false,
      symbols_to_index: [],
      token_separators: [],
      default_sorting_field: undefined,
      ...schema,
    },
    {
      excludeKeys: (k) => !hashCreateSchemaFields.has(k),
    },
  );

  const fieldsToHash = fields
    ?.filter((f) => f.name !== 'id')
    ?.map((f) => ({
      facet: false,
      index: true,
      infix: false,
      locale: '',
      optional: false,
      sort: sortableTypes.has(f.type),
      ...f,
    }));

  const fieldsHash = hash(
    { fields: fieldsToHash },
    {
      unorderedArrays: true,
    },
  );
  return `${schemaHash}_${fieldsHash}`;
};

export const applyMigrations = async (schema: CollectionCreateSchema) => {
  const existingCollection = await typesense.collections(schema.name).retrieve();

  const hashs = [hashCreateSchema(schema), hashCreateSchema(existingCollection)];

  if (hashs[0] === hashs[1]) {
    return existingCollection;
  }

  logger.info(`Migration needed for ${schema.name}...`);
  return createCollection(schema, existingCollection);
};
